import 'package:flutter/material.dart';
import 'package:weather/models/location_model.dart';
import 'package:intl/intl.dart';
import 'package:weather/api/network_api.dart';
import 'package:weather/models/forecast_model.dart';
import 'package:weather/models/weather_model.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Location> locations = [ Location(city: "Montpellier", country: "France", lat: "43.6109", lon: "3.8772")];
    return MaterialApp(
        title: 'Weather Forecast',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        debugShowCheckedModeBanner: false,
        home: CurrentWeatherPage(locations, context)
    );
  }

}

class CurrentWeatherPage extends StatefulWidget {
  final List<Location> locations;
  final BuildContext context;

  CurrentWeatherPage(this.locations, this.context);

  @override
  _CurrentWeatherPageState createState() => _CurrentWeatherPageState(this.locations, this.context);
}

class _CurrentWeatherPageState extends State<CurrentWeatherPage> {
  final List<Location> locations;
  final Location location;
  final BuildContext context;

  _CurrentWeatherPageState(List<Location> locations, BuildContext context)
      : this.locations = locations,
        this.context = context,
        this.location = locations[0];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white70,
        body: ListView(children: <Widget>[
          currentWeatherViews(locations, location, context),
          forcastViewsHourly(this.location),
          forcastViewsDaily(this.location),
        ]));
  }

  Widget currentWeatherViews(List<Location> locations, Location location,
      BuildContext context) {
    WeatherForeCastModel _weather;

    return FutureBuilder(builder: (context, snapshot) {
      if (snapshot.hasData) {
        _weather = snapshot.data as WeatherForeCastModel;
        if (_weather == null) {
          return const Text("Error getting weather");
        } else {
          return Column(children: [
            createCityName(locations, location, context),
            weatherBox(_weather),
            weatherDetailsBox(_weather),
          ]);
        }
      } else {
        return const Center(child: CircularProgressIndicator());
      }
    },
      future: Network.getCurrentWeather(location),
    );
  }

  Widget forcastViewsHourly(Location location) {
    Forecast _forcast;

    return FutureBuilder(builder: (context, snapshot) {
      if (snapshot.hasData) {
        _forcast = snapshot.data as Forecast;
        if (_forcast == null) {
          return const Text("Error getting weather");
        } else {
          return hourlyBoxes(_forcast);
        }
      } else {
        return const Center(child: CircularProgressIndicator());
      }
    },
      future: Network.getForecast(location),
    );
  }

  Widget forcastViewsDaily(Location location) {
    Forecast _forcast;

    return FutureBuilder(
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          _forcast = snapshot.data as Forecast;
          if (_forcast == null) {
            return const Text("Error getting weather");
          } else {
            return dailyBoxes(_forcast);
          }
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
      future: Network.getForecast(location),
    );
  }

  Widget weatherBox(WeatherForeCastModel _weather) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.all(10.0),
          margin: const EdgeInsets.all(10.0),
          height: 125.0,
          width: 150,
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(40))),
          child: Row(children: [
            Column(
                children: <Widget>[
                  Container(
                      child: Text(
                        "${_weather.temp.toInt()}°",
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 60,
                            color: Colors.black),
                      )),

                  Container(
                      margin: const EdgeInsets.all(5.0),
                      child: Text(
                        "Max.${_weather.high.toInt()}° Min.${_weather.low
                            .toInt()}°",
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 15,
                            color: Colors.black),
                      )),
                ])
          ]),
        ),
      ],
    );
  }



  Widget weatherDetailsBox(WeatherForeCastModel _weather) {
    return Container (
        padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10, right: 15),
        margin: const EdgeInsets.only(left: 15, top: 0, bottom: 10, right: 15),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10))
        ),
        child: Column(
            children: [
              Container(
                child: Text("Additional  Information",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 15,
                      color: Colors.black),),
              ),
              Container(
                padding: const EdgeInsets.only(left: 1, top: 20, bottom: 1, right: 10),
                margin: const EdgeInsets.only(left: 10, top: 0, bottom: 1, right: 10),
                child: Row(
                  children: [
                    Expanded(
                        child: Column(
                          children: [
                            Container(
                                child: const Text(
                                  "Humidity",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 12,
                                      color: Colors.black),
                                )),
                            Container(
                                child: Text(
                                  "${_weather.humidity.toInt()}%",
                                  textAlign: TextAlign.left,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 13,
                                      color: Colors.black),
                                ))
                          ],
                        )),
                    Expanded(
                        child: Column(
                          children: [
                            Container(
                                child: const Text(
                                  "Pressure",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 12,
                                      color: Colors.black),
                                )),
                            Container(
                                child: Text(
                                  "${_weather.pressure} hPa",
                                  textAlign: TextAlign.left,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 13,
                                      color: Colors.black),
                                ))
                          ],
                        )),

                    Expanded(
                        child: Column(
                          children: [
                            Container(
                                child: const Text(
                                  "Wind",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 12,
                                      color: Colors.black),
                                )),
                            Container(
                                child: Text(
                                  "${_weather.wind} km/h",
                                  textAlign: TextAlign.left,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 13,
                                      color: Colors.black),
                                ))
                          ],
                        )),
                    Expanded(
                        child: Column(
                          children: [
                            Container(
                                child: const Text(
                                  "Feels like",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 12,
                                      color: Colors.black),
                                )),
                            Container(
                                child: Text(
                                  "${_weather.feelsLike.toInt()}°",
                                  textAlign: TextAlign.left,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 13,
                                      color: Colors.black),
                                ))
                          ],
                        )),
                  ],
                ),
              ),
            ]
        ));
  }


  Widget hourlyBoxes(Forecast _forecast) {
    return Container(
        height: 120.0,
        child: ListView.builder(
            padding:
            const EdgeInsets.only(left: 7, top: 1, bottom: 20, right: 7),
            scrollDirection: Axis.horizontal,
            itemCount: _forecast.hourly.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      width: 2,
                      color: Colors.grey,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Column(children: [
                    Text(
                      "${getTimeFromTimestamp(_forecast.hourly[index].dt)}",
                      style: const TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 14,
                          color: Colors.black),
                    ),
                    getWeatherIcon(_forecast.hourly[index].icon),
                    Text(
                      "${_forecast.hourly[index].temp}°",
                      style: const TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 17,
                          color: Colors.black),
                    ),
                  ]));
            }));
  }


  Widget dailyBoxes(Forecast _forcast) {
    return Expanded(
        child: ListView.builder(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            padding:
            const EdgeInsets.only(left: 8, top: 0, bottom: 0, right: 8),
            itemCount: _forcast.daily.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      width: 1,
                      color: Colors.grey,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  padding: const EdgeInsets.only(
                      left: 15, top: 5, bottom: 5, right: 15),
                  margin: const EdgeInsets.all(0),
                  child: Row(children: [
                    Expanded(
                        child: Text(
                          "${getDateFromTimestamp(_forcast.daily[index].dt)}",
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        )),
                    Expanded(
                        child: getWeatherIcon(_forcast.daily[index].icon)),
                    Expanded(
                        child: Text(
                          "${_forcast.daily[index].high.toInt()}° ----- ${_forcast
                              .daily[index].low.toInt()}°",
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        )),
                  ]));
            }));
  }


  Widget createCityName(List<Location> locations, Location location, BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(
            top: 40, left: 25.0, bottom: 0, right: 40.0
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text.rich(
              TextSpan(
                  text: '${location.city.capitalizeFirstOfEach}',
                  style:
                  TextStyle(fontSize: 25)),
            ),],
        ));
  }

  Image getWeatherIcon(String _icon) {
    String path = 'assets/icons/';
    String imageExt = ".png";
    return Image.asset(path + _icon + imageExt, width: 50, height: 50,);
  }

  String getDateFromTimestamp(int timestamp) {
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    var formatter = DateFormat('E');
    return formatter.format(date);
  }

  String getTimeFromTimestamp(int timestamp) {
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    var formatter = DateFormat('h a');
    //var formatter = DateFormat.Hm();

    return formatter.format(date);
  }
}


extension StringExtension on String {
  String get inCaps => '${this[0].toUpperCase()}${this.substring(1)}';
  String get allInCaps => this.toUpperCase();
  String get capitalizeFirstOfEach =>
      this.split(" ").map((str) => str.inCaps).join(" ");
}
