import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:weather/models/location_model.dart';
import 'package:weather/models/weather_model.dart';

class WeatherRepo {
  static String lang = "fr";
  static String units = "metric";
  static String cnt = "7";
  static String appid = "c99e3381dc106b8a121f8385fde77852";

  Future<WeatherForeCastModel> getWeather(Location location) async {
    String cityName = location.city;
    var finalUrl = "https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=$appid&units=$units&lang=$lang";

    var response = await http.Client().get(Uri.parse(finalUrl));

    if (response.statusCode != 200) throw Exception();

    return WeatherForeCastModel.fromJson(jsonDecode(response.body));
  }
}