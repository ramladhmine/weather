import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:weather/models/forecast_model.dart';
import 'package:weather/models/location_model.dart';
import 'package:weather/models/weather_model.dart';

class Network {
  static String lang = "fr";
  static String units = "metric";
  static String cnt = "7";
  static String appid = "c99e3381dc106b8a121f8385fde77852";

  static Future<WeatherForeCastModel> getWeatherForecast({required String cityName}) async {
    var finalUrl ="https://api.openweathermap.org/data/2.5/forecast?q=$cityName&lang=$lang&units=$units&cnt=$cnt&appid=$appid";

    final response = await http.get(Uri.parse(finalUrl));
    print("URL : ${Uri.encodeFull(finalUrl)}");

    if (response.statusCode == 200) {
      print("weather data: ${response.body}}");
      return WeatherForeCastModel.fromJson(json.decode(response.body));
    } else {
      throw Exception("Error getting weather forecast");
    }
  }

  static Future getCurrentWeather(Location location) async {
    String cityName = location.city;
    var finalUrl = "https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=$appid&units=$units&lang=$lang";

    final response = await http.get(Uri.parse(finalUrl));

    if (response.statusCode == 200) {
      return WeatherForeCastModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception("Error getting current weather");
    }
  }

  static Future getForecast(Location location) async {
    String lat = location.lat;
    String lon = location.lon;
    var finalUrl = "https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&appid=$appid&units=$units&lang=$lang";

    final response = await http.get(Uri.parse(finalUrl));
    print("URL : ${Uri.encodeFull(finalUrl)}");

    if (response.statusCode == 200) {
      print("weather data: ${response.body}}");
      return Forecast.fromJson(jsonDecode(response.body));
    } else {
      throw Exception("Error getting forecast");
    }
  }

}